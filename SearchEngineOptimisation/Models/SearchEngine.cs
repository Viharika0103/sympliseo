﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace SearchEngineOptimisation.Models
{
    // table columns for SearchEngine
    public class SearchEngine
    {
        [Key]
        public int ID { get; set; }        
        public string SEOName { get; set; }
        public string Status { get; set; }

        
    }

    // table columns for SearchRange
    public class SearchRange
    {
        [Key]
        public int ID { get; set; }
        public int SEORange { get; set; }
        public int Status { get; set; }
    }

    public class SEOInputInput
    {
        public List<SearchEngine> searchEngines { get; set; }
        public List<SearchRange> searchRanges { get; set; }
    }


}
