﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace SearchEngineOptimisation.Migrations
{
    public partial class Adddata : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "SearchEngines",
                columns: new[] { "ID", "SEOName", "Status" },
                values: new object[,]
                {
                    { 1, "Google", "1" },
                    { 2, "Bing", "1" }
                });

            migrationBuilder.InsertData(
                table: "SearchRanges",
                columns: new[] { "ID", "SEORange", "Status" },
                values: new object[,]
                {
                    { 1, 10, 1 },
                    { 2, 50, 1 },
                    { 3, 100, 1 },
                    { 4, 150, 1 }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "SearchEngines",
                keyColumn: "ID",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "SearchEngines",
                keyColumn: "ID",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "SearchRanges",
                keyColumn: "ID",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "SearchRanges",
                keyColumn: "ID",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "SearchRanges",
                keyColumn: "ID",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "SearchRanges",
                keyColumn: "ID",
                keyValue: 4);
        }
    }
}
