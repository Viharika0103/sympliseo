﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using SearchEngineOptimisation.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;

namespace SearchEngineOptimisation.Context
{
    public class SearchEngineContext:DbContext
    {
        public SearchEngineContext(DbContextOptions<SearchEngineContext> options):base(options)
        {

        }

         public DbSet<SearchEngine> SearchEngines { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<SearchEngine>().HasData(
                 new SearchEngine
                 {
                     ID =1,
                     SEOName = "Google",
                     Status  = "1"
                 },
                 new SearchEngine
                 {
                     ID =2,
                     SEOName = "Bing",
                     Status = "1"
                 }

                );

            modelBuilder.Entity<SearchRange>().HasData(
                 new SearchRange
                 {
                     ID = 1,
                     SEORange = 10,
                     Status = 1
                 },
                 new SearchRange
                 {
                     ID = 2,
                     SEORange = 50,
                     Status = 1
                 },
                 new SearchRange
                 {
                     ID = 3,
                     SEORange = 100,
                     Status = 1
                 },
                 new SearchRange
                 {
                     ID = 4,
                     SEORange = 150,
                     Status = 1
                 }

                );

        }
        public DbSet<SearchRange> SearchRanges { get; set; }

        

    }
}
