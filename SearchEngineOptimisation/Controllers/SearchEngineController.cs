﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using SearchEngineOptimisation.Context;
using SearchEngineOptimisation.Models;
using SearchEngineOptimisation.Services;

namespace SearchEngineOptimisation.Controllers
{
    public class SearchEngineController : Controller
    {
        // private readonly SearchEngineContext _context;
        private readonly ILogger<SearchEngineController> _logger;
        private ISEOService _seoService;

        public SearchEngineController( ISEOService seoService, ILogger<SearchEngineController> logger)
        {
          //  _context = context;
            _seoService = seoService;
            _logger = logger;
           
        }

        // to get searchengine and range
        // GET: SearchEngine
        public IActionResult Index()
        {
            
            var viewModel = new SEOInputInput();
            viewModel.searchEngines = _seoService.GetSEOList();
            viewModel.searchRanges = _seoService.GetSearchRangeList();
            return View(viewModel);
        }

        [HttpGet]
        public JsonResult GetRanking(int range, string searchengine, string keyword, string findwebsite)
        {
            _logger.LogInformation("Get Ranking from SEO" + searchengine);

            var ranking = _seoService.GetSympliSearch(range, searchengine , keyword, findwebsite);


            return new JsonResult(ranking);

        }

     
    }
}
