﻿using Microsoft.EntityFrameworkCore;
using SearchEngineOptimisation.Context;
using SearchEngineOptimisation.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using System.Text;
using System.Net;
using System.IO;
using System.Text.RegularExpressions;

namespace SearchEngineOptimisation.Services
{
    public interface ISEOService
    {
        List<SearchEngine> GetSEOList();
        List<SearchRange> GetSearchRangeList();
        string GetSympliSearch
            (int searchrange, string searchengine , string keySearch, string Website);

    }
    public class SEOService: ISEOService
    {
        private SearchEngineContext _context;
        private readonly ILogger<SEOService> _logger;

        public SEOService(SearchEngineContext context, ILogger<SEOService> logger)
        {
            _context = context;
            _logger  = logger;
        }

        public List<SearchEngine> GetSEOList()
        {
            return _context.SearchEngines
                            .Where(x => x.Status == "1")
                            .ToList();


        }

        public List<SearchRange> GetSearchRangeList()
        {
            return _context.SearchRanges
                            .Where(x => x.Status == 1)
                            .ToList();


        }

        public string GetSympliSearch(int searchrange, string searchengine, string keySearch, string Website)
        {
            //count of url having http and https
            int urlcount = 0;

            //all the url with word infotract
            List<int> ranklist = new List<int>();


            // inorder to search each page of SEO incrementing and passing the page number dynamically
            for (int i = 1; i <= 10; i++)
            {

                if (urlcount <= searchrange) // to search for the range
                {

                    _logger.LogInformation(urlcount.ToString());

                    string SearchResults = "";
                    StringBuilder sb = new StringBuilder();
                    byte[] ResultsBuffer = new byte[8192];
                    string txtKeyWords = keySearch;

                    // as the page numbers in the search links are prefixed by 0 as default for single digits
                    if (searchengine == "Google")
                    {
                        SearchResults = "http://" + searchengine + ".com/search?q=" + txtKeyWords.Trim() + "?page=" + i + "&num=30";

                    }
                    // As we have only two search engine we have use else, if more use 
                    else
                    {
                        if (i == 1)
                        {

                            SearchResults = "http://" + searchengine + ".com/search?q=" + txtKeyWords.Trim() + "&first=" + i;
                        }
                        else
                        {
                            // first paramenter in bing uses the number of the result link to load; i.e by default search engine loads 10 result per each page first = 11 result page is page 2
                            // inorder to load each page 
                            int j = 10; // default result in page
                            int k = j - i; // every time in increments we need to page the link number with in the page i.e in page to we need to pass link number betwwen 11 to 20
                            i = i + k;
                            SearchResults = "http://" + searchengine + ".com/search?q=" + txtKeyWords.Trim() + "&first=" + i;
                        }
                    }
                    HttpWebRequest request = (HttpWebRequest)WebRequest.Create(SearchResults);
                    HttpWebResponse response = (HttpWebResponse)request.GetResponse();

                    _logger.LogInformation(response.ToString());

                    Stream resStream = response.GetResponseStream();
                    string tempString = null;
                    int count = 0;
                    do
                    {
                        count = resStream.Read(ResultsBuffer, 0, ResultsBuffer.Length);
                        if (count != 0)
                        {
                            tempString = Encoding.ASCII.GetString(ResultsBuffer, 0, count);
                            sb.Append(tempString);
                        }
                    }

                    while (count > 0);
                    string LinksHTML = sb.ToString();

                    //HTML of the search engine requested
                    _logger.LogInformation(LinksHTML);

                    // ExtractingLinks                
                    if (LinksHTML.Contains("<a href=") || LinksHTML.Contains("/url?q="))
                    {

                        var matches = (Regex.Matches(LinksHTML, "<a href=(.*?) |<a href=(.*?) h="));

                        foreach (Match item in matches)
                        {
                            string node = item.ToString();
                            //   Console.WriteLine(text);  // outputs HtmlAgilityPack.HtmlNode instead o th actual link!!

                            string link = node.ToString();

                            if (link.ToString().Contains("www.") && link.ToString().Contains("http://") || link.ToString().Contains("https://"))
                            {
                                urlcount++; // to get the the ranking of the URL
                                if (urlcount <= searchrange) // to display requested search range 
                                {
                                    if (link.ToString().Contains(Website))
                                    {
                                        ranklist.Add(urlcount); // get the rank of the URL with www.http:// or www.https://infortract....

                                    }
                                }

                            }


                        }

                    }
                }
            }
            return string.Join(",", ranklist.Select(n => n.ToString()).ToArray());
        }


    }
}
