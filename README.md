# Dev Environment Setup
- Install dotnetcore 3.1: https://www.microsoft.com/net/core#windows
- Install visual studio 2019 : https://visualstudio.microsoft.com/downloads/


# Download and install all packages needed for front end
$ npm install

# Download required .Net libraries and build the code base
$ dotnet clean
$ dotnet restore
$ dotnet build

#download nuget package

Microsoft.EntityFrameworkCore
Microsoft.EntityFrameworkCore.SqlServer
Microsoft.EntityFrameWorkCore.Tools

# links used to get the SEO for both search engine:

#Google

https://Google.com/search?q=”e-settlements”?page=1&num=30

q= query string (i.e e- settlements)
page = page number of the google search
num = count of result in each page of search engine

#bing

https://Google.com/search?q=”e-settlements”&first=11

q= query string (i.e e- settlements)
first = 11 result page with is page 2



# App URL
http://localhost:5000/

Can be changed in solution 
Right click the project  properties  Debug in the left pane  Web Server Setting in right pane App URL

#code code is hard for query string and search engine as we have only 2;we can make it dynamic by using SQl.


#Connection String
Change connection string in appsettings.json

"ConnectionStrings": {
    "DefaultConnection": "Server="ServerName";Database="DataBaseName";User ID= "ID";Password="Password";Allow User Variables=true;"
  }
  
WindowsAuthendication
"ConnectionStrings": {
    "DefaultConnection": "Server="ServerName";Database="DataBaseName";Trusted_Connection=True;MultipleActiveResultSets=true;"
  }
  
#Migration
Use Package manager console Tools-->Nuget Package Manager --> Package Manager Console.
Get-Help about_entityframeworkcore -- give you a list of commands that we can use
Get-Help add-migration -- give you a list of commands realated to add migration

To Create initial migration
Add-migration "Name"
Update-Database


